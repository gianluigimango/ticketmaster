/**
 *	@author Luigi
 *	Endpoint fetches RSS Streams depending on request
 */
var express = require('express');
var request = require('request');

var router = express.Router();

router.get('/getFeed', function(req, res) {

	/**
	 *	URL use Google Feed API
	 */
	var execute = function(url) {
		var url = 'http://ajax.googleapis.com/ajax/services/feed/load?v=2.0&num=200&q=' + encodeURIComponent(url);

		request(url, function(err, rep, body) {
			if (err) {
				console.error(err);
			} else {
				console.info('[RSS] ' + req.query.type);

				res.json(JSON.parse(body));
			}
		});
	}

	if (req.query && req.query.type) {
		switch(req.query.type) {
			case 'dlm': execute('http://www.dailymail.co.uk/articles.rss'); break;
			case 'sky': execute('http://feeds.skynews.com/feeds/rss/world.xml'); break;
			case 'bbc': execute('http://feeds.bbci.co.uk/news/rss.xml?edition=uk'); break;

			default: console.error('RSS Feed not found');
		}
	} else {
		console.error('Parameters not specified correctly');
	}

});

module.exports = router;