/**
 *	@author Luigi
 *	Gulpfile compile SASS to CSS
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
 
gulp.task('default', function () {
	return gulp.src('./client/*.scss')
    	.pipe(sass().on('error', sass.logError))
    	.pipe(gulp.dest('./client/'));
});