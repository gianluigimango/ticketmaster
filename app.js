/**
 *	@author Luigi
 *	Main App Bootstrap
 *	@contains API routing
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var api = require('./server/api.js');

app.listen('8000', function() {
	console.info('[App] Browse at: localhost:8000');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', express.static(__dirname));

app.use('/api', api);

module.exports = app;