/**
 *	@author Luigi
 *	Main Module
 *	Render template
 */
(function(factory) {
	define(['client/collection/feeds.js'], factory);
}(function(feeds) {
	'use strict';

	var module = {

		init: function() {
			var _this = this;

			var def = $.Deferred();

			feeds.fetch(def);

			def.done(function(res) {
				_this.collection = res;
				console.log(res);

				$.get('client/templates/mainTemplate.html').done(function(out) {
					_this.template = _.template(out);

					_this.render();
				});
			});
		},

		render: function() {
			$('.main-container').html(this.template({collection: this.collection}));
		}

	}

	return module.init();

}));