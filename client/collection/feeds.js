/**
 *	@author Luigi
 *	Collection fetch content from API
 */
define(function() {

	return {

		urls: [
			'sky',
			'dlm',
			'bbc'
		],

		data: [],

		fetch: function(def) {
			var _this = this;

			_.each(this.urls, function(url) {

				$.get('/api/getFeed', {type: url}).then(function(res) {
					if (res && res.responseStatus === 200) {
						res.responseData.feed.type = url;

						_this.data.push(res.responseData.feed);
					}
				}).then(function() {
					if (_this.data.length === _this.urls.length) return def.resolve(_this.data);
				});

			});
		}

	}

});