# README #

Documentation

### What is this repository for? ###

* Ticket Master Test

### How do I get set up? ###

* Run node app.js to serve our single page application.

### Comments ###

* I tried not to use too many external libraries in the front end as requested, only a few have been used: 
 . JQuery
 . Underscore
 . Require

### Description ###

I have specified a bootstrap app.js file that will create a server and serve our application on a specific port, then served the /api route with its endpoint /getFeed in an external server file, for better maintenance.

Our client requests the API for the RSS feeds and creates a collection to hold all items to then be rendered on a template. I have used the underscore template engine to handle our collection. I have appended anchors on our category to point and go to a specific section on user interaction.

I have chosen SASS as a pre-compiler and GULP.

There are some improvements to be made on this, that can be discussed.